﻿using Modul2Test_Vladimir_Asic.Interfaces;
using Modul2Test_Vladimir_Asic.Models;
using Modul2Test_Vladimir_Asic.Repository;
using Modul2Test_Vladimir_Asic.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Modul2Test_Vladimir_Asic.Controllers
{
    public class ArtiklController : Controller
    {
        private IArtiklRepository aRepository = new ArtiklRepository();
        private IKategorijaArtiklaRepository kaRepository = new KategorijaArtiklaRepository();

        // GET: Artikl
        public ActionResult Index()
        {
            ArtiklKategorijeArtiklaViewModels model = new ArtiklKategorijeArtiklaViewModels();
            model.Artikli = aRepository.GetAll();
            foreach (var item in model.Artikli)
            {
                var res = kaRepository.GetById(item.KategorijaArtikla.Id);
                item.KategorijaArtikla = res;
            }
            model.KategorijeArtikla = kaRepository.GetAll();
            //model.ProsecnaOcena = pRepository.ProsecnaOcena();
            //model.ProsecnaOcena = model.Piva.Average(p => p.Ocena);
            model.Artikli = model.Artikli.Where(a => a.Aktivan).ToList();
            return View(model);
        }

        // POST: Artikl
        [HttpPost]
        public ActionResult Create(ArtiklKategorijeArtiklaViewModels model)
        {
            model.Artikl.KategorijaArtikla = kaRepository.GetById(model.selectedKategorijaArtiklaId);
            if (ModelState.IsValid)
            {
                //model.Artikl.KategorijaArtikla.Id = model.selectedKategorijaArtiklaId;
                var res = aRepository.Create(model.Artikl);
                if (res)
                {
                    return RedirectToAction("Index");
                }
            }

            model.Artikli = aRepository.GetAll();
            foreach (var item in model.Artikli)
            {
                var res = kaRepository.GetById(item.KategorijaArtikla.Id);
                item.KategorijaArtikla = res;
            }
            model.KategorijeArtikla = kaRepository.GetAll();

            return View("Index", model);
        }

        // GET: Pivo
        [HttpPost]
        public ActionResult Delete(int id)
        {
            Artikl artikl = new Artikl();
            artikl = aRepository.GetById(id);
            artikl.Aktivan = false;
            if (ModelState.IsValid)
            {
                var res = aRepository.Update(artikl);
                if (res)
                {
                    return RedirectToAction("Index");
                }
            }

            ArtiklKategorijeArtiklaViewModels model = new ArtiklKategorijeArtiklaViewModels();
            model.Artikli = aRepository.GetAll();
            foreach (var item in model.Artikli)
            {
                var res = kaRepository.GetById(item.KategorijaArtikla.Id);
                item.KategorijaArtikla = res;
            }
            model.KategorijeArtikla = kaRepository.GetAll();
            model.Artikli = model.Artikli.Where(a => a.Aktivan).ToList();
            return View("Index", model);
        }

        // GET: Artikl/1
        public ActionResult Edit(int id)
        {
            ArtiklKategorijeArtiklaViewModels model = new ArtiklKategorijeArtiklaViewModels();
            model.Artikl = aRepository.GetById(id);
            model.KategorijeArtikla = kaRepository.GetAll();
            return View(model);
        }

        // POST: Artikl/1
        [HttpPost]
        public ActionResult Edit(ArtiklKategorijeArtiklaViewModels model)
        {
            model.Artikl.KategorijaArtikla = kaRepository.GetById(model.selectedKategorijaArtiklaId);
            if (ModelState.IsValid)
            {
                var res = aRepository.Update(model.Artikl);
                if (res)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }

        // GET: Artikl/1
        public ActionResult Filter(ArtiklKategorijeArtiklaViewModels model)
        {
            if (!model.Filtriraj)
            {
                return RedirectToAction("Index");
            }
            else
            {
                model.Artikli = aRepository.GetAll();
                model.Artikli = model.Artikli.Where(a => a.Aktivan).ToList();
                model.Artikli = model.Artikli.Where(a => a.Akcija == true).ToList();
                foreach (var item in model.Artikli)
                {
                    var res = kaRepository.GetById(item.KategorijaArtikla.Id);
                    item.KategorijaArtikla = res;
                }
                model.KategorijeArtikla = kaRepository.GetAll();
                return View("Index", model);
            }
        }
    }
}