﻿using Modul2Test_Vladimir_Asic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul2Test_Vladimir_Asic.Interfaces
{
    interface IArtiklRepository
    {
        List<Artikl> GetAll();
        Artikl GetById(int id);
        bool Create(Artikl artikl);
        bool Update(Artikl artikl);
        bool Delete(int id);
    }
}
