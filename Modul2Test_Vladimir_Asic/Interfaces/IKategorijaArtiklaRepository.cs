﻿using Modul2Test_Vladimir_Asic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul2Test_Vladimir_Asic.Interfaces
{
    interface IKategorijaArtiklaRepository
    {
        List<KategorijaArtikla> GetAll();
        KategorijaArtikla GetById(int id);
    }
}
