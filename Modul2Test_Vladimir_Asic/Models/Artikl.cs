﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Modul2Test_Vladimir_Asic.Models
{
    public enum Tip
    {
        Muski,
        Zenski,
        Deciji
    }

    public class Artikl
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Niste uneli naziv")]
        public string Naziv { get; set; }

        public Tip Tip { get; set; }

        [Required(ErrorMessage = "Niste uneli cenu")]
        [Range(0.1, Double.MaxValue, ErrorMessage = "Vrednost mora biti veca od 0")]
        public decimal Cena { get; set; }

        [Required(ErrorMessage = "Niste uneli marku")]
        public string Marka { get; set; }

        [Required(ErrorMessage = "Niste uneli opis")]
        [StringLength(200, MinimumLength = 3, ErrorMessage = "Uneli ste manje od 10 karaktera")]
        public string Opis { get; set; }

        public KategorijaArtikla KategorijaArtikla { get; set; }
        public bool Aktivan { get; set; } = true;
        public bool Akcija { get; set; }

        public Artikl()
        {
            this.KategorijaArtikla = new KategorijaArtikla();
        }

        public Artikl(int id, string naziv, Tip tip, decimal cena, string marka, string opis, bool aktivan, bool akcija) : this()
        {
            this.Id = id;
            this.Naziv = naziv;
            this.Tip = tip;
            this.Cena = cena;
            this.Marka = marka;
            this.Opis = opis;
            this.Aktivan = aktivan;
            this.Akcija = akcija;
        }
    }
}