﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Modul2Test_Vladimir_Asic.Models
{
    public class KategorijaArtikla
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Niste uneli naziv")]
        public string Naziv { get; set; }
        //public List<Artikl> Artikl { get; set; }

        public KategorijaArtikla()
        {
            //this.Artikl = new List<Artikl>();
        }

        public KategorijaArtikla(int id, string naziv) /*: this()*/
        {
            this.Id = id;
            this.Naziv = naziv;
        }
    }
}