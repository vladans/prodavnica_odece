﻿using Modul2Test_Vladimir_Asic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Modul2Test_Vladimir_Asic.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Modul2Test_Vladimir_Asic.Repository
{
    public class ArtiklRepository : IArtiklRepository
    {
        private SqlConnection conn;
        private void Connection()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ProdavnicaOdeceConnection"].ConnectionString;
            conn = new SqlConnection(connectionString);
        }

        public bool Create(Artikl artikl)
        {
            try
            {
                string query = "INSERT INTO ARTIKL (NAZIV_ARTIKLA, TIP_ARTIKLA, CENA_ARTIKLA, MARKA_ARTIKLA, OPIS_ARTIKLA, AKTIVAN_ARTIKL, AKCIJA_ARTIKL, KATEGORIJA_ARTIKLA_ID) VALUES (@NAZIV_ARTIKLA, @TIP_ARTIKLA, @CENA_ARTIKLA, @MARKA_ARTIKLA, @OPIS_ARTIKLA, @AKTIVAN_ARTIKL, @AKCIJA_ARTIKL, @KATEGORIJA_ARTIKLA_ID);";
                query += " SELECT SCOPE_IDENTITY()";        // selektuj id novododatog zapisa nakon upisa u bazu

                Connection();   // inicijaizuj novu konekciju

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@NAZIV_ARTIKLA", artikl.Naziv);
                    cmd.Parameters.AddWithValue("@TIP_ARTIKLA", artikl.Tip.ToString());
                    cmd.Parameters.AddWithValue("@CENA_ARTIKLA", artikl.Cena);
                    cmd.Parameters.AddWithValue("@MARKA_ARTIKLA", artikl.Marka);
                    cmd.Parameters.AddWithValue("@OPIS_ARTIKLA", artikl.Opis);
                    cmd.Parameters.AddWithValue("@AKTIVAN_ARTIKL", artikl.Aktivan);
                    cmd.Parameters.AddWithValue("@AKCIJA_ARTIKL", artikl.Akcija);
                    cmd.Parameters.AddWithValue("@KATEGORIJA_ARTIKLA_ID", artikl.KategorijaArtikla.Id);

                    conn.Open();                            // otvori konekciju
                    var newId = cmd.ExecuteScalar();        // izvrsi upit nad bazom, vraca id novododatog zapisa
                    conn.Close();                           // zatvori konekciju

                    if (newId != null)
                    {
                        return true;    // upis uspesan, generisan novi id
                    }
                }
                return false;   // upis bezuspesan
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom upisa novog artikla. " + ex.StackTrace);  // ispis u output-prozorcicu
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<Artikl> GetAll()
        {
            List<Artikl> artikli = new List<Artikl>();
            try
            {
                //string query = "SELECT * FROM Autos ORDER BY AutoId";
                string query = @"SELECT * FROM Artikl a, KATEGORIJA_ARTIKLA ka
                                WHERE a.KATEGORIJA_ARTIKLA_ID = ka.KATEGORIJA_ARTIKLA_ID ORDER BY ARTIKL_ID";

                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dataAdapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dataAdapter.Fill(ds, "Artikli");   // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["Artikli"];         // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                    conn.Close();                   // zatvori konekciju
                }
                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int id = int.Parse(dataRow["ARTIKL_ID"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string naziv = dataRow["NAZIV_ARTIKLA"].ToString();
                    //Tip tip = (Tip)int.Parse(dataRow["TIP_ARTIKLA"].ToString());
                    string tipString = dataRow["TIP_ARTIKLA"].ToString();
                    Enum.TryParse(tipString, out Tip tip);
                    decimal cena = decimal.Parse(dataRow["CENA_ARTIKLA"].ToString());
                    string marka = dataRow["MARKA_ARTIKLA"].ToString();
                    string opis = dataRow["OPIS_ARTIKLA"].ToString();
                    bool aktivan = bool.Parse(dataRow["AKTIVAN_ARTIKL"].ToString());
                    bool akcija = bool.Parse(dataRow["AKCIJA_ARTIKL"].ToString());
                    int kategorijaArtiklaId = int.Parse(dataRow["KATEGORIJA_ARTIKLA_ID"].ToString());
                    //string nazivKategorijeArtikla = dataRow["NAZIV_TIPA"].ToString();

                    Artikl artikl = new Artikl(id, naziv, tip, cena, marka, opis, aktivan, akcija);
                    artikl.KategorijaArtikla.Id = kategorijaArtiklaId;
                    //artikl.kategorijaArtikla.Naziv = nazivKategorijeArtikla;
                    artikli.Add(artikl);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom izlistavanja artikla. {ex.StackTrace}");
                throw ex;
            }
            return artikli;
        }

        public Artikl GetById(int id)
        {
            try
            {
                string query = @"
                    SELECT *
                    FROM ARTIKL a
                    INNER JOIN KATEGORIJA_ARTIKLA ka
                    ON a.KATEGORIJA_ARTIKLA_ID = ka.KATEGORIJA_ARTIKLA_ID
                    WHERE ARTIKL_ID = @ARTIKL_ID;";

                Connection();
                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@ARTIKL_ID", id);

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    //pribavimo bookstorove
                    dadapter.Fill(ds, "Artikl");
                    dt = ds.Tables["Artikl"];
                    conn.Close();

                    if (dt.Rows.Count == 1)
                    {
                        int artiklId = int.Parse(dt.Rows[0]["ARTIKL_ID"].ToString());    // iz svake kolone datog reda izvuci vrednost
                        string naziv = dt.Rows[0]["NAZIV_ARTIKLA"].ToString();
                        //Tip tip = (Tip)int.Parse(dataRow["TIP_ARTIKLA"].ToString());
                        string tipString = dt.Rows[0]["TIP_ARTIKLA"].ToString();
                        Enum.TryParse(tipString, out Tip tip);
                        decimal cena = decimal.Parse(dt.Rows[0]["CENA_ARTIKLA"].ToString());
                        string marka = dt.Rows[0]["MARKA_ARTIKLA"].ToString();
                        string opis = dt.Rows[0]["OPIS_ARTIKLA"].ToString();
                        bool aktivan = bool.Parse(dt.Rows[0]["AKTIVAN_ARTIKL"].ToString());
                        bool akcija = bool.Parse(dt.Rows[0]["AKCIJA_ARTIKL"].ToString());
                        int kategorijaArtiklaId = int.Parse(dt.Rows[0]["KATEGORIJA_ARTIKLA_ID"].ToString());

                        Artikl artikl = new Artikl(artiklId, naziv, tip, cena, marka, opis, aktivan, akcija);
                        artikl.KategorijaArtikla.Id = kategorijaArtiklaId;
                        return artikl;
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom dobavljanja artikla po Id-ju. " + ex.StackTrace);  // ispis u outputprozoru
                throw ex;
            }
        }

        public bool Update(Artikl artikl)
        {
            bool res = false;
            try
            {
                string query = @"
                    UPDATE ARTIKL
                    SET NAZIV_ARTIKLA = @NAZIV_ARTIKLA, TIP_ARTIKLA = @TIP_ARTIKLA, CENA_ARTIKLA = @CENA_ARTIKLA, MARKA_ARTIKLA = @MARKA_ARTIKLA, OPIS_ARTIKLA = @OPIS_ARTIKLA, AKTIVAN_ARTIKL = @AKTIVAN_ARTIKL, AKCIJA_ARTIKL = @AKCIJA_ARTIKL, KATEGORIJA_ARTIKLA_ID = @KATEGORIJA_ARTIKLA_ID
                    WHERE ARTIKL_ID = @ARTIKL_ID;";

                Connection();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@ARTIKL_ID", artikl.Id);
                    cmd.Parameters.AddWithValue("@NAZIV_ARTIKLA", artikl.Naziv);
                    cmd.Parameters.AddWithValue("@TIP_ARTIKLA", artikl.Tip.ToString());
                    cmd.Parameters.AddWithValue("@CENA_ARTIKLA", artikl.Cena);
                    cmd.Parameters.AddWithValue("@MARKA_ARTIKLA", artikl.Marka);
                    cmd.Parameters.AddWithValue("@OPIS_ARTIKLA", artikl.Opis);
                    cmd.Parameters.AddWithValue("@AKTIVAN_ARTIKL", artikl.Aktivan);
                    cmd.Parameters.AddWithValue("@AKCIJA_ARTIKL", artikl.Akcija);
                    cmd.Parameters.AddWithValue("@KATEGORIJA_ARTIKLA_ID", artikl.KategorijaArtikla.Id);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    res = true;
                    return res;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom azuriranja artikla. " + ex.StackTrace);  // ispis u output prozoru
                //return res;
                throw ex;
            }
        }
    }
}