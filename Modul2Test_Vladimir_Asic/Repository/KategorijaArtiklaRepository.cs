﻿using Modul2Test_Vladimir_Asic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Modul2Test_Vladimir_Asic.Models;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Modul2Test_Vladimir_Asic.Repository
{
    public class KategorijaArtiklaRepository : IKategorijaArtiklaRepository
    {
        private SqlConnection conn;
        private void Connection()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ProdavnicaOdeceConnection"].ConnectionString;
            conn = new SqlConnection(connectionString);
        }

        public List<KategorijaArtikla> GetAll()
        {
            List<KategorijaArtikla> kategorijeArtikla = new List<KategorijaArtikla>();
            try
            {
                string query = "SELECT * FROM KATEGORIJA_ARTIKLA ORDER BY KATEGORIJA_ARTIKLA_ID";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dataAdapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dataAdapter.Fill(ds, "KategorijeArtikla"); // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["KategorijeArtikla"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                    conn.Close();                  // zatvori konekciju
                }
                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int id = int.Parse(dataRow["KATEGORIJA_ARTIKLA_ID"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string naziv = dataRow["NAZIV_TIPA"].ToString();

                    kategorijeArtikla.Add(new KategorijaArtikla(id, naziv));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom izlistavanja svih kategorija artikla. {ex.StackTrace}");
                throw ex;
            }
            return kategorijeArtikla;
        }

        public KategorijaArtikla GetById(int id)
        {
            try
            {
                //string query = "SELECT * FROM VRSTA_PIVA vp INNER JOIN PIVO p ON vp.VRSTA_PIVA_ID = p.VRSTA_PIVA_ID WHERE VRSTA_PIVA_ID = @VrstaPivaId;";
                string query = "SELECT * FROM KATEGORIJA_ARTIKLA WHERE KATEGORIJA_ARTIKLA_ID = @KategorijaArtiklaId;";
                Connection();
                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@KategorijaArtiklaId", id);

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    //pribavimo bookstorove
                    dadapter.Fill(ds, "KategorijeArtikla");
                    dt = ds.Tables["KategorijeArtikla"];
                    conn.Close();

                    if (dt.Rows.Count == 1)
                    {
                        int kategorijaArtiklaId = int.Parse(dt.Rows[0]["KATEGORIJA_ARTIKLA_ID"].ToString());
                        string naziv = dt.Rows[0]["NAZIV_TIPA"].ToString();

                        return new KategorijaArtikla(kategorijaArtiklaId, naziv);
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom dobavljanja kategorije artikla po Id-ju. " + ex.StackTrace);  // ispis u outputprozoru
                throw ex;
            }
        }
    }
}