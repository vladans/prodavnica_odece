﻿$(document).ready(function () {
    $("#forma1").submit(function (event) {

        //var artiklAkcija = $("#Artikl_Akcija").val();
        var artiklNaziv = $("#Artikl_Naziv").val();
        var artiklCenaString1 = $("#Artikl_Cena").val();
        var artiklCenaString = artiklCenaString1.replace(',', '.').replace(' ', '');
        //var artiklCena = parseFloat(artiklCenaString);
        var artiklMarka = $("#Artikl_Marka").val();
        var artiklOpis = $("#Artikl_Opis").val();
        //var artiklTip = $("#Artikl_Tip").val();

        // NAZIV
        if (!artiklNaziv) {
            event.preventDefault();
            //$("#PivoNazivSpan").html("Custom message: This is required field.").css("color", "red");
            $("#naziv1")
                .html("JQuery validacija: Niste uneli naziv.")
                .css(
                {
                    "color": "red",
                    //"font-size": 20
                })
        }
        else {
            $("#naziv1").html("");
        }

        // CENA
        if (artiklCenaString == null || artiklCenaString == "" || artiklCenaString == 'undefined') {
            event.preventDefault();
            $("#cena1").html("JQuery validacija: Niste uneli cenu.").css("color", "red");
        }
        else {
            $("#cena1").html("");

            if (!$.isNumeric(artiklCenaString)) {
                event.preventDefault();
                $("#cena1").html("JQuery validacija: Unesite broj.").css("color", "red");
            }
            else {
                $("#cena1").html("");

                if (artiklCenaString < 0.1 /*|| pivoOcena > 10*/) {
                    event.preventDefault();
                    $("#cena1").html("JQuery validacija: Vrednost mora biti veca od 0.").css("color", "red");
                }
                else {
                    $("#cena1").html("");
                }
            }
        }

        // MARKA
        if (!artiklMarka) {
            event.preventDefault();
            $("#marka1").html("JQuery validacija: Niste uneli marku.").css("color", "red");
        }
        else {
            $("#marka1").html("");
        }

        // OPIS
        if (!artiklOpis) {
            event.preventDefault();
            $("#opis1").html("JQuery validacija: Niste uneli opis.").css("color", "red");
        }
        else {
            $("#opis1").html("");

            if (artiklOpis.length < 10) {
                event.preventDefault();
                $("#opis1").html("JQuery validacija: Uneli ste manje od 10 karaktera.").css("color", "red");
            }
        }
    });
});