﻿using Modul2Test_Vladimir_Asic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modul2Test_Vladimir_Asic.ViewModels
{
    public class ArtiklKategorijeArtiklaViewModels
    {
        public Artikl Artikl { get; set; }
        public List<Artikl> Artikli { get; set; }
        public List<KategorijaArtikla> KategorijeArtikla { get; set; }
        public int selectedKategorijaArtiklaId { get; set; }
        public bool Filtriraj { get; set; }
    }
}